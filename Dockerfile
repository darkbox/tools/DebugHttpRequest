FROM php:8.1.0-cli

EXPOSE 80

ENV OUTPUT_FORMAT=HTML

COPY my-php.ini /usr/local/etc/php/conf.d/my-php.ini

WORKDIR /app/

COPY debugger.php debugger.php

ENTRYPOINT ["php", "-S", "0.0.0.0:80", "debugger.php"]