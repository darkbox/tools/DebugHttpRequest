<?php
ob_start();

$Formatters = [];

$Formatters["HTML"] = function ($obj) {
    
?><!DOCTYPE xhtml>
<html>
    <head>
        <title>Debug Http Request</title>
        <style>
            h5 {
                border: 1px dashed gray;
                padding: 3px;
            }
        </style>
    </head>
    <body>
        <table>
            <th>
                <td colspan="2">Value</td>
            </th>
            <tr>
                <td>HOST_NAME</td>
                <td><?php echo $obj->HOST_NAME;?></td>
            </tr>
            <tr>
                <td>REQUEST_URI</td>
                <td><?php echo $obj->REQUEST_URI;?></td>
            </tr>
            <tr>
                <td>REMOTE_ADDR</td>
                <td><?php echo $obj->REMOTE_ADDR;?></td>
            </tr>
            <tr>
                <td>REMOTE_PORT</td>
                <td><?php echo $obj->REMOTE_PORT;?></td>
            </tr>
        </table>
        <div>
            <h5>Header:</h5>
            <pre><code><?php foreach($obj->HEADER as $n=>$v) echo "$n: $v\n\r";?></code></pre>
        </div>
        <div>
            <h5>Query:</h5>
            <pre><code><?php echo http_build_query($obj->QUERY);?></code></pre>
        </div>
        <div>
            <h5>Body:</h5>
            <pre><code><?php echo $obj->BODY;?></code></pre>
        </div>
    </body>
</html>
<?php
    
    $content = ob_get_contents();
    ob_clean();
    header("Content-Type: text/html", true, 200);
    return $content;
};
$Formatters["JSON"] = function ($obj) {
    header("Content-Type: application/json", true, 200);
    return json_encode($obj, JSON_PRETTY_PRINT);
};


ob_clean();
exit (
    (new class () {
        function set ($key, $val) {
            $this->$key = $val;
            return $this;
        }
        function show ($format = "HTML") {
            global $Formatters;
            $format = strtoupper(trim($format));
            if (array_key_exists($format, $Formatters)) {
                $formatter = $Formatters[$format];
                return $formatter($this);
            }
            return "Env::OUTPUT_FORMAT is invalid. ( Default value: HTML )";
        }
    })
        // ->set("SERVER", $_SERVER)
        ->set("HOST_NAME", $_SERVER["HTTP_HOST"])
        ->set("REQUEST_URI", $_SERVER["REQUEST_URI"])
        ->set("REMOTE_ADDR", $_SERVER["REMOTE_ADDR"])
        ->set("REMOTE_PORT", $_SERVER["REMOTE_PORT"])

        ->set("HEADER", apache_request_headers())

        ->set("QUERY", $_GET)
        ->set("BODY", file_get_contents('php://input'))
        ->show(isset($_ENV["OUTPUT_FORMAT"]) ? $_ENV["OUTPUT_FORMAT"] : "HTML")
);
